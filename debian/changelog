r-cran-sjmisc (2.8.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Wed, 03 Jul 2024 09:10:21 +0900

r-cran-sjmisc (2.8.9-2) unstable; urgency=medium

  * Rebuild with r-graphics-engine set by r-base
    Closes: 1039662
  * Standards-Version: 4.6.2 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Lintian-overrides (see lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Thu, 06 Jul 2023 11:00:40 +0200

r-cran-sjmisc (2.8.9-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-cran-sjlabelled.

 -- Andreas Tille <tille@debian.org>  Fri, 10 Dec 2021 11:42:35 +0100

r-cran-sjmisc (2.8.7-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-cran-sjlabelled.

 -- Andreas Tille <tille@debian.org>  Fri, 20 Aug 2021 10:09:07 +0200

r-cran-sjmisc (2.8.6-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jan 2021 17:41:46 +0100

r-cran-sjmisc (2.8.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Sat, 06 Jun 2020 08:00:28 +0200

r-cran-sjmisc (2.8.4-3) unstable; urgency=medium

  * Team Upload.
  * Update Test Depends (Closes: #961499)
  * Bump debhelper-compat version to 13

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 26 May 2020 10:57:37 +0530

r-cran-sjmisc (2.8.4-2) unstable; urgency=medium

  * Source upload in R 4.0 transition

 -- Andreas Tille <tille@debian.org>  Tue, 19 May 2020 13:35:35 +0200

r-cran-sjmisc (2.8.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Add salsa-ci file (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sun, 05 Apr 2020 18:29:34 +0200

r-cran-sjmisc (2.8.3-2) unstable; urgency=medium

  * Team Upload.
  * Update test dependencies
  * Add "Rules-Requires-Root:no"
  * Update upstream/metadata
  * Bump standards version to 4.5.0

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 07 Mar 2020 10:22:47 +0530

r-cran-sjmisc (2.8.3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1
  * Set upstream metadata fields: Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Tue, 14 Jan 2020 12:05:40 +0100

r-cran-sjmisc (2.8.2-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * dh-update-R to update Build-Depends
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database.

 -- Andreas Tille <tille@debian.org>  Tue, 01 Oct 2019 11:07:57 +0200

r-cran-sjmisc (2.8.1-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * dh-update-R to update Build-Depends
  * Standards-Version: 4.4.0
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Wed, 17 Jul 2019 13:10:55 +0200

r-cran-sjmisc (2.7.7-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Mon, 07 Jan 2019 14:50:19 +0100

r-cran-sjmisc (2.7.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Thu, 22 Nov 2018 07:24:47 +0100

r-cran-sjmisc (2.7.5-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Fri, 21 Sep 2018 10:52:29 +0200

r-cran-sjmisc (2.7.4-1) unstable; urgency=medium

  * Initial release (closes: #899997)

 -- Andreas Tille <tille@debian.org>  Sun, 09 Sep 2018 22:27:37 +0200
